Webform Service Server
======================

Description
-----------

This module allows web forms to be submitted to an XMLRPC web service.

This module is designed to work with Webform 3 (http://drupal.org/project/webform).

Installation
------------

Enable the module and it addes a tabs on each webform to enable and choose a xmlrpc target for sending all webform submittion to a given service.

License
-------

Copyright (C) 2011  David Pascoe-Deslauriers, Coldfront Labs Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


