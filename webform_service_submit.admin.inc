<?php

/**
 * @file
 * Administrative forms for webform_service_submit.
 *
 * @author David Pascoe-Deslauriers <dpascoed@coldfrontlabs.ca>
 * @copyright  2011 Coldfront Labs Inc.
 * @license http://coldfrontlabs.ca/LICENSE.txt
 */

/**
 * Implements hook_admin_settings().
 */
function webform_service_submit_admin_settings($form, &$form_state) {
  $form['webform_service_submit_add_service_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Service Endpoint'),
    '#default_value' => isset($form_state['values']['webform_service_submit_add_service_endpoint'])?
				$form_state['values']['webform_service_submit_add_service_endpoint']:
				variable_get('webform_service_submit_add_service_endpoint', ''),
    '#description' => t('Enter the URL of the service endpoint (e.g. https://example.com/service)'),
  );

  $form['#submit'][] = 'webform_service_submit_admin_settings_submit';
  $form = system_settings_form($form);

  return $form;
}

/**
 * Implements hook_validate().
 */
function webform_service_submit_admin_settings_validate($form, &$form_state) {
  $endpoint = $form_state['values']['webform_service_submit_add_service_endpoint'];
  if (!preg_match('/^http(s){0,1}:\/\/\S+(:\d+){0,1}$/', $endpoint)) {
    form_set_error('webform_service_submit_add_service_endpoint', t('Invalid endpoint'));
  }

}

/**
 * Implements hook_submit().
 */
function webform_service_submit_admin_settings_submit($form, &$form_state) {
  $endpoint = $form_state['values']['webform_service_submit_add_service_endpoint'];
  variable_set('webform_service_submit_add_service_endpoint', $endpoint);
}
